package br.com.jar.kiki.business.exception;

/**
 * Excecao pai excecoes de relacionadas a negocio.
 * 
 * @author Raphael Costa
 *
 */
public class BusinessException extends Exception {

	private static final long serialVersionUID = 5576629830844628643L;

	public BusinessException() {
		super();
	}

	public BusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException(Throwable cause) {
		super(cause);
	}

}
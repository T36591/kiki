package br.com.jar.kiki.to;

import java.io.Serializable;

/**
 * TransferObject que representa a importacao de dados de rota.
 * 
 * @author Raphael Costa
 *
 */
public class RotaImportTO implements Serializable {

	private static final long serialVersionUID = 8272456904105453764L;

	private String origem;

	private String destino;

	private Double distancia;

	public RotaImportTO() {
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Double getDistancia() {
		return distancia;
	}

	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}

}
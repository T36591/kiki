package br.com.jar.kiki.data.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;

@Entity
@Table(name = "MALHA_LOGISTICA")
public class MalhaLogistica implements Serializable {

	private static final long serialVersionUID = -4210233237040877991L;

	@EmbeddedId
	private PK pk;

	@Column(name = "VLR_DISTANCIA", columnDefinition = "DECIMAL(10,2)")
	private Double vlrDistancia;

	public MalhaLogistica() {
	}

	public MalhaLogistica(PK pk) {
		this.pk = pk;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (pk == null ? 0 : pk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}

		if (!(obj instanceof MalhaLogistica)) {
			return false;
		}

		final MalhaLogistica other = (MalhaLogistica) obj;
		if (pk == null) {
			if (other.pk != null) {
				return false;
			}
		} else if (!pk.equals(other.pk)) {
			return false;
		}
		return true;
	}

	@Embeddable
	public static class PK implements Serializable {

		private static final long serialVersionUID = -7461747454153188589L;

		@Column(name = "NOM_DESTINO", length = 60)
		private String nomDestino;

		@Column(name = "NOM_ORIGEM", length = 60)
		private String nomOrigem;

		@Column(name = "NOM_MAPA", length = 60)
		private String nomMapa;

		public PK() {
		}

		public PK(String nomMapa, String nomOrigem, String nomDestino) {
			this.nomMapa = nomMapa;
			this.nomOrigem = nomOrigem;
			this.nomDestino = nomDestino;
		}

		@Override
		public int hashCode() {
			if (nomMapa == null && nomOrigem == null && nomDestino == null) {
				return 0;
			}

			return nomMapa.hashCode() ^ nomOrigem.hashCode() ^ nomDestino.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}

			if (!(obj instanceof PK)) {
				return false;
			}

			final PK other = (PK) obj;

			return StringUtils.equals(nomMapa, other.nomMapa) && StringUtils.equals(nomOrigem, other.nomOrigem) && StringUtils.equals(nomDestino, other.nomDestino);
		}

		public String getNomDestino() {
			return nomDestino;
		}

		public void setNomDestino(String nomDestino) {
			this.nomDestino = nomDestino;
		}

		public String getNomOrigem() {
			return nomOrigem;
		}

		public void setNomOrigem(String nomOrigem) {
			this.nomOrigem = nomOrigem;
		}

		public String getNomMapa() {
			return nomMapa;
		}

		public void setNomMapa(String nomMapa) {
			this.nomMapa = nomMapa;
		}

	}

	public PK getPk() {
		return pk;
	}

	public void setPk(PK pk) {
		this.pk = pk;
	}

	public Double getVlrDistancia() {
		return vlrDistancia;
	}

	public void setVlrDistancia(Double vlrDistancia) {
		this.vlrDistancia = vlrDistancia;
	}
}
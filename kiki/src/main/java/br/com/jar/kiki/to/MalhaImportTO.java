package br.com.jar.kiki.to;

import java.io.Serializable;
import java.util.List;

/**
 * Representa a requisicao de importacao de uma malha logistica.
 * 
 * @author Raphael Costa
 *
 */
public class MalhaImportTO implements Serializable {

	private static final long serialVersionUID = 4104944001089463919L;

	private String mapa;

	private List<RotaImportTO> rotas;

	public MalhaImportTO() {
	}

	public String getMapa() {
		return mapa;
	}

	public void setMapa(String mapa) {
		this.mapa = mapa;
	}

	public List<RotaImportTO> getRotas() {
		return rotas;
	}

	public void setRotas(List<RotaImportTO> rotas) {
		this.rotas = rotas;
	}

}

package br.com.jar.kiki.business.crud;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import br.com.jar.kiki.business.exception.BusinessException;
import br.com.jar.kiki.data.dao.MalhaLogisticaRepository;
import br.com.jar.kiki.data.dao.MalhaLogisticaRepository.RotaNNiveisRowMapper;
import br.com.jar.kiki.data.entities.MalhaLogistica;
import br.com.jar.kiki.data.entities.MalhaLogistica.PK;

/**
 * Objeto de negocio que encapsula acesso aos dados e que contem logicas crud com relacao a {@link MalhaLogistica}.
 * 
 * @author Raphael Costa
 *
 */
@Service
public class MalhaLogisticaBO {

	@Resource
	private MalhaLogisticaRepository repo;

	public MalhaLogistica insert(MalhaLogistica entity) throws BusinessException {
		try {
			return repo.save(entity);
		} catch (Exception e) {
			throw new BusinessException("malhaLogisticaBO.insert.error", e);
		}
	}

	public int deleteByNomMapa(String mapa) throws BusinessException {
		try {
			return repo.deleteByPk_nomMapa(mapa);
		} catch (Exception e) {
			throw new BusinessException("malhaLogisticaBO.deleteByNomMapa.error", e);
		}
	}

	public MalhaLogistica findByPk(PK pk) throws BusinessException {
		try {
			return repo.findOne(pk);
		} catch (Exception e) {
			throw new BusinessException("malhaLogisticaBO.findByPk.error", e);
		}
	}

	public List<List<MalhaLogistica>> findRotaByNivel(String mapa, String origem, String destino, int qtdNiveis) throws BusinessException {
		final List<Object> rotasNNiveis;

		if (qtdNiveis == 2) {
			rotasNNiveis = repo.findRotas2Niveis(mapa, origem, destino);

		} else if (qtdNiveis == 3) {
			rotasNNiveis = repo.findRotas3Niveis(mapa, origem, destino);

		} else {
			throw new BusinessException("malhaLogisticaBO.rotaNNiveis.niveis.max");
		}

		final List<List<MalhaLogistica>> ret = new ArrayList<List<MalhaLogistica>>();
		for (int i = 0; i < rotasNNiveis.size(); i++) {

			try {
				final RotaNNiveisRowMapper rowMapper = MalhaLogisticaRepository.RotaNNiveisRowMapper.getInstance();
				final Object[] campos = (Object[]) rotasNNiveis.get(i);
				final List<MalhaLogistica> rotas = rowMapper.mapRow(campos);
				ret.add(rotas);

			} catch (Exception e) {
				throw new BusinessException("malhaLogisticaBO.rotaNNiveis.rowmapper.error", e);
			}
		}

		return ret;
	}

	public List<MalhaLogistica> findByMapaDestino(String mapa, String destino) throws BusinessException {
		try {
			return repo.findByPk_nomMapaAndPk_nomDestino(mapa, destino);
		} catch (Exception e) {
			throw new BusinessException("malhaLogisticaBO.findByMapaDestino.error", e);
		}
	}
}
package br.com.jar.kiki.data.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.jar.kiki.data.entities.MalhaLogistica;
import br.com.jar.kiki.data.entities.MalhaLogistica.PK;

/**
 * Repositorio especifico para a classe {@link MalhaLogistica}.
 * 
 * @author Raphael Costa
 *
 */
@Repository
public interface MalhaLogisticaRepository extends CrudRepository<MalhaLogistica, PK> {

	// -------------------------------QUERIES-------------------------------------------- //

	final String QUERY_2_NIVEIS = "SELECT m1.nom_mapa m1_mapa, m1.nom_origem m1_origem, m1.nom_destino m1_destino, m1.vlr_distancia m1_distancia, m2.nom_mapa m2_mapa, m2.nom_origem m2_origem, m2.nom_destino m2_destino, m2.vlr_distancia m2_distancia  "
			+ " FROM malha_logistica m1 INNER JOIN malha_logistica m2  on m2.nom_mapa = m1.nom_mapa  and m2.nom_origem = m1.nom_destino "
			+ " WHERE m1.nom_mapa = :nomMapa and m1.nom_origem = :nomOrigem and m2.nom_destino = :nomDestino ";

	final String QUERY_3_NIVEIS = "SELECT m1.nom_mapa m1_mapa, m1.nom_origem m1_origem, m1.nom_destino m1_destino, m1.vlr_distancia m1_distancia, "
			+ " m2.nom_mapa m2_mapa, m2.nom_origem m2_origem, m2.nom_destino m2_destino, m2.vlr_distancia m2_distancia,  "
			+ " m3.nom_mapa m3_mapa, m3.nom_origem m3_origem, m3.nom_destino m3_destino, m3.vlr_distancia m3_distancia  " //
			+ " FROM malha_logistica m1 " //
			+ " INNER JOIN malha_logistica m2  on m2.nom_mapa = m1.nom_mapa  and m2.nom_origem = m1.nom_destino and m2.nom_destino <> m1.nom_origem"
			+ " INNER JOIN malha_logistica m3  on m3.nom_mapa = m2.nom_mapa  and m3.nom_origem = m2.nom_destino and m3.nom_destino <> m2.nom_origem"
			+ " WHERE m1.nom_mapa = :nomMapa and m1.nom_origem = :nomOrigem and m3.nom_destino = :nomDestino ";

	// ---------------------------------------------------------------------------------- //

	@Query(value = QUERY_2_NIVEIS, nativeQuery = true)
	List<Object> findRotas2Niveis(@Param("nomMapa") String nomMapa, @Param("nomOrigem") String nomOrigem, @Param("nomDestino") String nomDestino);

	@Query(value = QUERY_3_NIVEIS, nativeQuery = true)
	List<Object> findRotas3Niveis(@Param("nomMapa") String nomMapa, @Param("nomOrigem") String nomOrigem, @Param("nomDestino") String nomDestino);

	int deleteByPk_nomMapa(String mapa);

	List<MalhaLogistica> findByPk_nomMapaAndPk_nomDestino(String mapa, String destino);

	/**
	 * Rowmapper para consultas com N niveis.
	 * 
	 * @author Raphael Costa
	 *
	 */
	public static final class RotaNNiveisRowMapper {

		private static final RotaNNiveisRowMapper INSTANCE = new RotaNNiveisRowMapper();

		private static final int TOTAL_CAMPOS_ENTIDADE = 4;

		private RotaNNiveisRowMapper() {
		}

		public final List<MalhaLogistica> mapRow(Object[] campos) throws Exception {
			final List<MalhaLogistica> ret = new ArrayList<MalhaLogistica>();

			int count = 0;
			for (int i = 0; i < (campos.length / TOTAL_CAMPOS_ENTIDADE); i++) {
				final MalhaLogistica malha = new MalhaLogistica();
				malha.setPk(new PK((String) campos[count++], (String) campos[count++], (String) campos[count++]));
				malha.setVlrDistancia(((BigDecimal) campos[count++]).doubleValue());

				ret.add(malha);
			}

			return ret;
		}

		public static RotaNNiveisRowMapper getInstance() {
			return INSTANCE;
		}
	}

}

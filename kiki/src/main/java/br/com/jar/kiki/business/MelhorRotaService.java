package br.com.jar.kiki.business;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import br.com.jar.kiki.business.crud.MalhaLogisticaBO;
import br.com.jar.kiki.business.exception.BusinessException;
import br.com.jar.kiki.data.entities.MalhaLogistica;
import br.com.jar.kiki.data.entities.MalhaLogistica.PK;
import br.com.jar.kiki.to.MelhorRotaTO;

/**
 * Esse servico tem como tarefa principal encontrar a melhor rota baseado no mapa, origem e destino informados, alem de
 * calcular o custo total com base na autonomia e custo informados.<br>
 * O calculo de custo utilizado e o seguinte: <br>
 * <code>custoLitro * ( distancia / autonomia ) = custoTotal</code>
 * 
 * @author Raphael Costa
 *
 */
@Service
public class MelhorRotaService {

	private static final Log LOG = LogFactory.getLog(MelhorRotaService.class);

	public static final MathContext ROUND_CONTEXT = new MathContext(4, RoundingMode.CEILING);

	@Resource
	private MalhaLogisticaBO malhaLogisticaBO;

	public MelhorRotaService() {
	}

	public MelhorRotaTO findMelhorRota(String mapa, String origem, String destino, BigDecimal autonomia, BigDecimal custo) throws BusinessException {

		MelhorRotaTO rota1Nivel = null;

		final MalhaLogistica rotaDireta = malhaLogisticaBO.findByPk(new MalhaLogistica.PK(mapa, origem, destino));
		if (rotaDireta != null) {
			rota1Nivel = new MelhorRotaTO();
			rota1Nivel.setMapa(mapa);
			rota1Nivel.setOrigem(origem);
			rota1Nivel.setDestino(destino);

			buildMelhorRota(rota1Nivel, autonomia, custo, false, rotaDireta);
			LOG.debug(MessageFormat.format("Melhor rota 1 nivel encontrada {1}, distanciaTotal {2}, custo {3}", rota1Nivel.getRota(), rota1Nivel.getDistanciaTotal(),
					rota1Nivel.getCustoTotal()));
		} else {
			LOG.debug("Melhor rota 1 nivel NAO ENCONTRADA");
		}

		final MelhorRotaTO rota2Niveis = buscaRotaPorQtdNivel(mapa, origem, destino, autonomia, custo, 2);

		final MelhorRotaTO rota3Niveis = buscaRotaPorQtdNivel(mapa, origem, destino, autonomia, custo, 3);

		final MelhorRotaTO rotaPlus3Niveis = buscarRotaPorPlus3Niveis(mapa, origem, destino, autonomia, custo);

		final boolean foundRotas = (rota1Nivel != null || rota2Niveis != null || rota3Niveis != null || rotaPlus3Niveis != null);
		if (foundRotas) {
			final MelhorRotaTO[] rotas = new MelhorRotaTO[] { rota1Nivel, rota2Niveis, rota3Niveis, rotaPlus3Niveis };
			Arrays.sort(rotas, new Comparator<MelhorRotaTO>() {
				private static final int IGUAIS = 0;
				private static final int REF_SUBIR = 1;
				private static final int REF_DESCER = -1;

				public int compare(MelhorRotaTO o1, MelhorRotaTO o2) {
					if (o1 == null) {
						if (o2 == null) {
							return IGUAIS;
						} else {
							return REF_SUBIR;
						}
					} else {
						if (o2 == null) {
							return REF_DESCER;
						} else {
							return o1.getDistanciaTotal().compareTo(o2.getDistanciaTotal());
						}
					}
				}
			});

			return rotas[0];
		}

		throw new BusinessException("melhorRotaService.findMelhorRota.notfound");
	}

	private MelhorRotaTO buscarRotaPorPlus3Niveis(String mapa, String origem, String destino, BigDecimal autonomia, BigDecimal custo) throws BusinessException {
		final List<List<MalhaLogistica>> rotasPlus3Niveis = new ArrayList<List<MalhaLogistica>>();

		final List<MalhaLogistica> malhasAteDestino = malhaLogisticaBO.findByMapaDestino(mapa, destino);
		for (int i = 0; i < malhasAteDestino.size(); i++) {
			final MalhaLogistica rotaAteDestino = malhasAteDestino.get(i);

			final List<List<MalhaLogistica>> subRotas = malhaLogisticaBO.findRotaByNivel(mapa, rotaAteDestino.getPk().getNomOrigem(), origem, 3);
			if (CollectionUtils.isNotEmpty(subRotas)) {

				for (List<MalhaLogistica> subSubRotas : subRotas) {
					Collections.reverse(subSubRotas);

					final MalhaLogistica rotaAteDestinoInvertido = new MalhaLogistica(
							new PK(rotaAteDestino.getPk().getNomMapa(), rotaAteDestino.getPk().getNomDestino(), rotaAteDestino.getPk().getNomOrigem()));
					rotaAteDestinoInvertido.setVlrDistancia(rotaAteDestino.getVlrDistancia());
					subSubRotas.add(rotaAteDestinoInvertido);
					rotasPlus3Niveis.add(subSubRotas);
				}
			}
		}

		LOG.debug(MessageFormat.format("Rotas com 3+ niveis encontradas, total {0}", rotasPlus3Niveis.size()));
		if (CollectionUtils.isNotEmpty(rotasPlus3Niveis)) {
			List<MalhaLogistica> melhorRota = buscaMelhorRota(rotasPlus3Niveis);

			MelhorRotaTO melhorRotaTO = new MelhorRotaTO();
			melhorRotaTO.setMapa(mapa);
			melhorRotaTO.setOrigem(origem);
			melhorRotaTO.setDestino(destino);
			buildMelhorRota(melhorRotaTO, autonomia, custo, true, melhorRota.toArray(new MalhaLogistica[melhorRota.size()]));

			LOG.debug(MessageFormat.format("Melhor rota 3+ niveis encontrada {0}, distanciaTotal {1}, custo {2}", melhorRotaTO.getRota(), melhorRotaTO.getDistanciaTotal(),
					melhorRotaTO.getCustoTotal()));

			return melhorRotaTO;
		}

		return null;
	}

	private MelhorRotaTO buscaRotaPorQtdNivel(String mapa, String origem, String destino, BigDecimal autonomia, BigDecimal custo, final int qtdNiveis) throws BusinessException {

		try {
			final List<List<MalhaLogistica>> rotasNNiveis = malhaLogisticaBO.findRotaByNivel(mapa, origem, destino, qtdNiveis);

			if (CollectionUtils.isNotEmpty(rotasNNiveis)) {
				MelhorRotaTO melhorRotaTO = new MelhorRotaTO();
				melhorRotaTO.setMapa(mapa);
				melhorRotaTO.setOrigem(origem);
				melhorRotaTO.setDestino(destino);

				LOG.debug(MessageFormat.format("Rotas de {0} niveis encontradas {1}", qtdNiveis, rotasNNiveis.size()));

				List<MalhaLogistica> melhorRota = buscaMelhorRota(rotasNNiveis);

				buildMelhorRota(melhorRotaTO, autonomia, custo, false, melhorRota.toArray(new MalhaLogistica[melhorRota.size()]));

				LOG.debug(MessageFormat.format("Melhor rota {0} niveis encontrada {1}, distanciaTotal {2}, custo {3}", qtdNiveis, melhorRotaTO.getRota(),
						melhorRotaTO.getDistanciaTotal(), melhorRotaTO.getCustoTotal()));
				return melhorRotaTO;

			} else {
				LOG.debug(MessageFormat.format("Melhor rota {0} niveis NAO ENCONTRADA", qtdNiveis));
			}

		} catch (Exception e) {
			throw new BusinessException("melhorRotaService.rotaNNiveis.error", e);
		}

		return null;
	}

	private List<MalhaLogistica> buscaMelhorRota(final List<List<MalhaLogistica>> rotasNNiveis) {
		List<MalhaLogistica> melhorRota = null;
		for (List<MalhaLogistica> listaRotas : rotasNNiveis) {
			if (melhorRota == null) {
				melhorRota = listaRotas;

			} else {
				Double distanciaAnterior = 0D;
				for (MalhaLogistica rota : melhorRota) {
					distanciaAnterior += rota.getVlrDistancia();
				}

				Double distanciaAtual = 0D;
				for (MalhaLogistica rota : listaRotas) {
					distanciaAtual += rota.getVlrDistancia();
				}

				if (distanciaAtual < distanciaAnterior) {
					melhorRota = listaRotas;
				}

			}
		}
		return melhorRota;
	}

	/**
	 * Constroi objeto de melhor rota baseado nas rotas informadas. Elas devem vir ordenadas no sentido de rota mesma,
	 * um ponto indicando o proximo ponto ou utilizar o boolean inverterOrigemDestino true (para os casos de 3+ niveis
	 * onde a busca eh feita de forma contraria [DESTINO->ORIGEM] ). Alem disso, calcula o custo total utilizando a
	 * formula indicada acima.
	 * 
	 * @param melhorRotaTO
	 * @param autonomia
	 * @param custo
	 * @param inverterOrigemDestino
	 * @param rotas
	 * @throws BusinessException
	 */
	private void buildMelhorRota(MelhorRotaTO melhorRotaTO, BigDecimal autonomia, BigDecimal custo, boolean inverterOrigemDestino, MalhaLogistica... rotas)
			throws BusinessException {

		if (ArrayUtils.isEmpty(rotas)) {
			throw new BusinessException("melhorRotaService.buildMelhorRota.rotas.null");
		}

		final String[] pontos = new String[rotas.length + 1];
		Double distanciaTotal = 0D;
		for (int i = 0; i < rotas.length; i++) {
			final MalhaLogistica rota = rotas[i];

			pontos[i] = (inverterOrigemDestino) ? rota.getPk().getNomDestino() : rota.getPk().getNomOrigem();
			pontos[i + 1] = (inverterOrigemDestino) ? rota.getPk().getNomOrigem() : rota.getPk().getNomDestino();

			distanciaTotal += rota.getVlrDistancia();
		}

		final BigDecimal distanciaTotalBD = BigDecimal.valueOf(distanciaTotal);
		final BigDecimal custoTotal = custo.multiply(distanciaTotalBD.divide(autonomia, RoundingMode.CEILING), ROUND_CONTEXT);

		melhorRotaTO.setPontos(pontos);
		melhorRotaTO.setCustoTotal(custoTotal);
		melhorRotaTO.setDistanciaTotal(distanciaTotal);
	}

}

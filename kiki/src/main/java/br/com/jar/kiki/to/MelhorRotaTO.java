package br.com.jar.kiki.to;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

/**
 * TransferObject que representa a decisao da melhor rota de uma mapa dado origem e destino.
 * 
 * @author Raphael Costa
 *
 */
public class MelhorRotaTO implements Serializable, Cloneable {

	private static final String APPEND_ROTA = " -> ";

	private static final long serialVersionUID = 7129738116742974668L;

	private String mapa;

	private String origem;

	private String destino;

	private String[] pontos;

	private Double distanciaTotal;

	private BigDecimal custoTotal;

	public MelhorRotaTO() {
	}

	/**
	 * Atraves dos pontos definidos que essa rota passa, constroi uma string com todos os pontos separados pela string
	 * {@link MelhorRotaTO}.APPEND_ROTA
	 * 
	 * @return
	 */
	public String getRota() {
		if (ArrayUtils.isEmpty(this.pontos)) {
			return StringUtils.EMPTY;
		}

		final StringBuilder ret = new StringBuilder();
		for (String ponto : pontos) {
			ret.append(ponto).append(APPEND_ROTA);
		}

		return ret.substring(0, ret.length() - APPEND_ROTA.length());
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		final MelhorRotaTO clone = new MelhorRotaTO();

		clone.mapa = mapa;
		clone.origem = origem;
		clone.destino = destino;
		clone.pontos = pontos;
		clone.distanciaTotal = distanciaTotal;
		clone.custoTotal = custoTotal;

		return clone;
	}

	public String getMapa() {
		return mapa;
	}

	public void setMapa(String mapa) {
		this.mapa = mapa;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Double getDistanciaTotal() {
		return distanciaTotal;
	}

	public void setDistanciaTotal(Double distanciaTotal) {
		this.distanciaTotal = distanciaTotal;
	}

	public BigDecimal getCustoTotal() {
		return custoTotal;
	}

	public void setCustoTotal(BigDecimal custoTotal) {
		this.custoTotal = custoTotal;
	}

	public String[] getPontos() {
		return pontos;
	}

	public void setPontos(String[] pontos) {
		this.pontos = pontos;
	}

}